#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include "stdio.h"

int main()
{
	int fd;
	char chain[56];
	char* nombre = "/tmp/FIFOlogger";

	if(mkfifo(nombre,0666) == 0)
	{
		fd = open(nombre, O_RDONLY);
		if(fd==-1) {
			perror("open");
			return 1;
		}
		
		while(read(fd,&chain,sizeof(chain)))
		{
			int l=0;
			for(int i=0;i<56;i++)
			{
				if(chain[i]!='\0')
					l++;
				else break;
			}
			char cad[l];
			for(int i=0; i<l;i++)
				cad[i]=chain[i];
			write(1, &cad,sizeof(cad));
		}
		
		close(fd);
		unlink(nombre);
	}
	else{
		perror("Error al crear la tubería");
	}
	return 1;
}

